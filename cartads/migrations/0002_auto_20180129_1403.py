# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cartads', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartads',
            name='application',
            field=models.CharField(default='CartADS', max_length=200, verbose_name='Application identifier'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartads',
            name='token_authorization',
            field=models.CharField(default='', max_length=128, verbose_name='Token Authorization'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartads',
            name='token_url',
            field=models.URLField(default='', max_length=256, verbose_name='Token URL'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartads',
            name='verify_cert',
            field=models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
        ),
        migrations.AddField(
            model_name='cartads',
            name='wsdl_url',
            field=models.CharField(default='', max_length=256, verbose_name='WSDL URL'),
            preserve_default=False,
        ),
    ]
