# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cartads', '0002_auto_20180129_1403'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cartads',
            options={'verbose_name': 'CartADS Webservices'},
        ),
    ]
