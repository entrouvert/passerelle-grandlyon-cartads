# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cartads', '0004_remove_cartads_application'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cartads',
            name='wsdl_url',
        ),
        migrations.AddField(
            model_name='cartads',
            name='wsdl_url_etapedossier',
            field=models.CharField(default='url', max_length=256, verbose_name='WSDL URL EtapeDossier'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartads',
            name='wsdl_url_portail',
            field=models.CharField(default='url', max_length=256, verbose_name='WSDL URL Portail'),
            preserve_default=False,
        ),
    ]
