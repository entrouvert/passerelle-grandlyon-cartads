# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cartads', '0005_auto_20180424_1616'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cartads',
            name='wsdl_url_etapedossier',
        ),
        migrations.RemoveField(
            model_name='cartads',
            name='wsdl_url_portail',
        ),
        migrations.AddField(
            model_name='cartads',
            name='cle_token_gfi',
            field=models.CharField(default='cle_token_gfi', max_length=128, verbose_name='Cle Token GFI'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartads',
            name='sendfile_ws_url',
            field=models.CharField(default='https://api-rec.grandlyon.com/ads-sendfile-rec/sendfile.aspx', max_length=256, verbose_name='sendfile ws url'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartads',
            name='wsdl_ads_dir_url',
            field=models.CharField(default='https://passerelle.guichet-recette.grandlyon.com/media/bap/cartads-rec/', max_length=256, verbose_name='WSDL ADS Dir URL'),
            preserve_default=False,
        ),
    ]
